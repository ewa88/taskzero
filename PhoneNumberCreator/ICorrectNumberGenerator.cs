namespace PhoneNumberCreator
{
    public interface ICorrectNumberGenerator
    {
        string GetNumberFromUser();
        char[] ChangeUserNumber(string number);
    }
}