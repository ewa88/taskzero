﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneNumberCreator
{
  public class CorrectNumberGenerator : ICorrectNumberGenerator
  {
        public string GetNumberFromUser()
        {
            string number = Console.ReadLine();
            while (String.IsNullOrEmpty(number))
            {
                Console.WriteLine("Wrtite correct number.");
                number = Console.ReadLine();
            }
            return number;
        }
        public char[] ChangeUserNumber(string number)
        {
            var wordWithLowerLetter = number.ToLower();
            var allChars = wordWithLowerLetter.ToCharArray();

            for (int i = 0; i < allChars.Length; i++)
            {

                if (allChars[i] == 'a' || allChars[i] == 'b' || allChars[i] == 'c')
                {
                    allChars[i] = '2';
                }
                if (allChars[i] == 'd' || allChars[i] == 'e' || allChars[i] == 'f')
                {
                    allChars[i] = '3';
                }
                if (allChars[i] == 'g' || allChars[i] == 'h' || allChars[i] == 'i')
                {
                    allChars[i] = '4';
                }
                if (allChars[i] == 'j' || allChars[i] == 'k' || allChars[i] == 'l')
                {
                    allChars[i] = '5';
                }
                if (allChars[i] == 'm' || allChars[i] == 'n' || allChars[i] == 'o')
                {
                    allChars[i] = '6';
                }
                if (allChars[i] == 'p' || allChars[i] == 'q' || allChars[i] == 'r' || allChars[i] == 's')
                {
                    allChars[i] = '7';
                }
                if (allChars[i] == 't' || allChars[i] == 'u' || allChars[i] == 'v')
                {
                    allChars[i] = '8';
                }
                if (allChars[i] == 'w' || allChars[i] == 'x' || allChars[i] == 'y' || allChars[i] == 'z')
                {
                    allChars[i] = '9';
                }
            }
            foreach (var singleChar in allChars)
            {
                Console.Write(singleChar);
            }
            Console.WriteLine();
            return allChars;
        }
    }
}
