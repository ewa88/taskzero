﻿using System;

namespace PhoneNumberCreator
{
   public class ProgramLoop
   {
        private ICorrectNumberGenerator _correctNumberGenerator;

        public ProgramLoop(ICorrectNumberGenerator correctNumberGenerator)
        {
            _correctNumberGenerator = correctNumberGenerator;
        }
        public void Execute()
        {
            bool loop = true;
            Console.WriteLine("Write number (if you want to end write exit).");
            while (loop)
            {
              string number = _correctNumberGenerator.GetNumberFromUser();

                if (number == "exit")
                {
                    loop = false;
                }
                else
                {
                    _correctNumberGenerator.ChangeUserNumber(number);
                }
            }
        }
    }
}
