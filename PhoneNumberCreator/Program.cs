﻿using System;

namespace PhoneNumberCreator
{
   public class Program
    {
        static void Main(string[] args)
        {
            new ProgramLoop(new CorrectNumberGenerator()).Execute();
        }
    }
}
