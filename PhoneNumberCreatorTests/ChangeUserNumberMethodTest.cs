﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhoneNumberCreator;

namespace PhoneNumberCreatorTests
{
    [TestClass]
    public class ChangeUserNumberMethodTest
    {
        [TestMethod]
        public void ChangeUserNumber_UserNumberInput_ChangedNumberOutput()
        {
            var generator = new CorrectNumberGenerator();

            //three examples of word with different size of letter
            //1
            string input = "0-800-BiKe";
            string expectedOutput = "0-800-2453";
            
            var arrayWithCorrectNumber = generator.ChangeUserNumber(input);
            CollectionAssert.AreEqual(expectedOutput.ToCharArray(), arrayWithCorrectNumber);

            //2
            string input2 = "0-800-10EMX";
            string expectedOutput2 = "0-800-10369";

            var arrayWithCorrectNumber2 = generator.ChangeUserNumber(input2);
            CollectionAssert.AreEqual(expectedOutput2.ToCharArray(), arrayWithCorrectNumber2);

            //3
            string input3 = "0-800-clvs10";
            string expectedOutput3 = "0-800-258710";

            var arrayWithCorrectNumber3 = generator.ChangeUserNumber(input3);
            CollectionAssert.AreEqual(expectedOutput3.ToCharArray(), arrayWithCorrectNumber3);
        }
    }
}
